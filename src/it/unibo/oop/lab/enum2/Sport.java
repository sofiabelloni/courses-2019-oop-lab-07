/**
 * 
 */
package it.unibo.oop.lab.enum2;

/**
 * Represents an enumeration for declaring sports.
 * 
 * 1) You must add a field keeping track of the number of members each team is
 * composed of (1 for individual sports)
 * 
 * 2) A second field will keep track of the name of the sport.
 * 
 * 3) A third field, of type Place, will allow to define if the sport is
 * practiced indoor or outdoor
 * 
 */
public enum Sport {

    /*
     * Declare the following sports:
     * 
     * - basket
     * - volley
     * - tennis
     * - bike
     * - F1
     * - motogp
     * - soccer
     * 
     */
	BASKET(Place.INDOOR, 5, "basket"),
	VOLLEY(Place.INDOOR, 6, "volley"),
	TENNIS(Place.OUTDOOR, 2, "tennis"),
	BIKE(Place.OUTDOOR, 1, "bike"),
	F1(Place.OUTDOOR, 1, "formula1"),
	MOTOGP(Place.OUTDOOR, 1, "motoGP"),
	SOCCER(Place.OUTDOOR, 11, "soccer");

    /*
     * [FIELDS]
     * 
     */
	
	private final Integer teamMembers;
	private final String actualtName;
	private final Place place;

    /*
     * [CONSTRUCTOR]
     * 
     * Constructor
     * 
     */
     Sport(final Place place, final int noTeamMembers, final String actualName){
    	 this.place = place;
    	 this.teamMembers = noTeamMembers;
    	 this.actualtName = actualName;
     }
     
	

    /*
     * [METHODS]
     * 
     * 1) public boolean isIndividualSport()
     * 
     * Must return true only if called on individual sports
     * 
     * 
     * 2) public boolean isIndoorSport()
     * 
     * Must return true in case the sport is practices indoor
     * 
     * 
     * 3) public Place getPlace()
     * 
     * Must return the place where this sport is practiced
     * 
     * 
     * 4) public String toString()
     * 
     * Returns the string representation of a sport
     */
     
     public boolean isIndividualSport() {
    	 return this.teamMembers == 1;
     }
     
     public boolean isIndoorSport() {
    	 return this.place.equals(Place.INDOOR);
     }
     
     public Place getPlace() {
    	 return this.place;
     }
     
     public String toString() {
    	 return "Sport - name: " + this.actualtName + ", number of members: " + this.teamMembers
    			 + ", place: " + this.place + " - ";
     }
}
