/**
 * 
 */
package it.unibo.oop.lab.enum2;

/**
 * Constant to define where sports are practiced
 */
public enum Place {
	INDOOR, OUTDOOR

}
